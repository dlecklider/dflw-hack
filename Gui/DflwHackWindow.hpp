#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#define WIN32_LEAN_AND_MEAN 

#include <QMainWindow>
#include <QMediaPlaylist>
#include <QMediaPlayer>
#include <QTimer>
#include <QLineEdit>
#include <QPointer>
#include <QShortcut>

#include <Windows.h>

#include "NamedPipeComms.hpp"

namespace Ui {
class DflwHackWindow;
}

// TODO: Add keyboard shortcuts to gui as well as backend
class DflwHackWindow : public QMainWindow
{
    Q_OBJECT

typedef void (DflwHackWindow::*ShortcutCallback)();

public:
    explicit DflwHackWindow(QWidget *parent = 0);
	~DflwHackWindow();

private slots:
	void toggle_HackEnable();
    void toggle_GodMode();
	void toggle_UnlimitedAmmo();
	void toggle_ESP();
	void on_button_click_About();
	void on_check_changed_Music(bool aNewState);

private:
	// Override QT nativeEvent function so that we can support native global keyboard shortcuts
	bool nativeEvent(const QByteArray& eventType, void* message, long* result);

	void SetupFormActions();
	void InitializeMediaPlayer(bool aMuteValue);
	bool FindDFLWDLL(QString& aPath);
	void SendMessageOverPipe(PipeMessageType aMessageHeader);
	void TurnOffHack();
	bool ConnectToHack();
	void dispatchGlobalShortcut(int aGlobalKeyboardShortcutID);

	void Gui_SetElementTextEnabled(QLineEdit* aLineEdit, bool aEnabled);
	void Gui_SetHackEnabledPicture(bool aEnabled);
	void Gui_SetHackOptionsEnabled(bool aEnabled);
	void Gui_ShowErrorPrompt(QString aErrorString);

	std::map<int, ShortcutCallback> mShortcutCallbackMap;
	QMediaPlaylist mMusicPlaylist;
	QMediaPlayer mMusicPlayer;
    Ui::DflwHackWindow* mUI;
	HANDLE mPipeHandle;
	bool mHackLoaded;
	bool mGodMode;
	bool mUnlimitedAmmo;
	bool mESP;
};

#endif // MAINWINDOW_H
