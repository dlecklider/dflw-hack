#include "Util.hpp"

PVOID util::GetInterfaceMethod(PVOID intf, DWORD methodIndex)
{
	return *(PVOID*)(*(DWORD*)intf + methodIndex * 4);
}

// Find Dereferenced Multi  
uintptr_t util::FindDMAAddr(uintptr_t aAddress, std::vector<unsigned int> aOffsets)
{
	for (unsigned int i = 0; i < aOffsets.size(); ++i)
	{
		aAddress = *(uintptr_t*)aAddress;
		aAddress += aOffsets[i];
	}

	return aAddress;
}

void util::WriteAddressOfLength(uintptr_t aAddress, uint64_t aVal, unsigned int aBytes, std::vector<unsigned int> aOffsets)
{
	uintptr_t writeAddress = FindDMAAddr(aAddress, aOffsets);

	switch (aBytes)
	{
	case(sizeof(uint8_t)):
		*(uint8_t*)writeAddress = aVal;
		break;
	case(sizeof(uint16_t)):
		*(uint16_t*)writeAddress = aVal;
		break;
	case(sizeof(uint32_t)):
		*(uint32_t*)writeAddress = aVal;
		break;
	case(sizeof(uint64_t)):
		*(uint64_t*)writeAddress = aVal;
		break;
	default:
		break;
	}
}

uint64_t util::ReadAddressOfLength(uintptr_t aAddress, unsigned int aBytes, std::vector<unsigned int> aOffsets)
{
	uintptr_t readAddress = FindDMAAddr(aAddress, aOffsets);
	uint64_t retVal;

	switch (aBytes)
	{
	case(sizeof(uint8_t)):
		retVal = *(uint8_t*)readAddress;
		break;
	case(sizeof(uint16_t)):
		retVal = *(uint16_t*)readAddress;
		break;
	case(sizeof(uint32_t)):
		retVal = *(uint32_t*)readAddress;
		break;
	case(sizeof(uint64_t)):
		retVal = *(uint64_t*)readAddress;
		break;
	default:
		break;
	}

	return retVal;
}