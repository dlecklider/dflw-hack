#ifndef DX7_MGR_HPP
#define DX7_MGR_HPP

#include "windows.h"
#include "d3d.h"

namespace dx7
{
	LPDIRECT3DDEVICE7 GetD3Dim700Device();

	void HookDevice(LPDIRECT3DDEVICE7 aDevice);
	void UnhookDevice(LPDIRECT3DDEVICE7 aDevice);

	void DrawBorderBox(int x, int y, int w, int h, int thickness, D3DCOLOR Colour, LPDIRECT3DDEVICE7 pDevice);
	void DrawFilledRect(int x, int y, int w, int h, D3DCOLOR color, LPDIRECT3DDEVICE7 dev);
};

#endif