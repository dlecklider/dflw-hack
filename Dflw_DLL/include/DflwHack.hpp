#ifndef DFLWHACK_HPP
#define DFLWHACK_HPP

#include "map"

class DflwHack
{
public:
	DflwHack();
	~DflwHack();

	void Init();
	void Update();

	void toggleGodMode() { mGodModeInput = !mGodModeInput; }
	void toggleUnlimitedAmmo() { mUnlimitedAmmoInput = !mUnlimitedAmmoInput; }
	void toggleESP() { mESPInput = !mESPInput; }

private:
	int  getPlayerHealth();
	void setPlayerHealth(int aVal);
	void setUnlimitedAmmo();
	void setDrawESP(bool aEnabled);
	
	void resetAmmoCounts();

	std::map<uintptr_t, int> mAmmoCountMap;

	const uintptr_t mPlayerBasePtr;
	bool mInitialValuesSet;

	// We have additional booleans as we need to know when the signal from the user has "gone high" or "gone low" to hook or unhook accordingly.
	bool mGodModeInput;
	bool mGodModeEnabled;
	int  mPreGodModeHealth;

	bool mUnlimitedAmmoInput;
	bool mUnlimitedAmmoEnabled;

	bool mESPInput;
	bool mESPEnabled; 
};

#endif // !DFLWHACK_HPP
