#include "DflwHackWindow.hpp"
#include "ui_dflwhackwindow.h"

#include <QMessageBox>
#include <QFileInfo>

#include <stdio.h>
#include <conio.h>
#include <tchar.h>

#include "DLLInjector.hpp"

DflwHackWindow::DflwHackWindow(QWidget *parent) :
    QMainWindow(parent),
	mShortcutCallbackMap(),
	mMusicPlaylist(),
	mMusicPlayer(),
    mUI(new Ui::DflwHackWindow),
	mPipeHandle(INVALID_HANDLE_VALUE),
	mHackLoaded(false),
	mGodMode(false),
	mUnlimitedAmmo(false),
	mESP(false)
{
	mUI->setupUi(this);

	// Set the window flags such that the application does not allow the user to resize the window.
	setWindowFlags(windowFlags() | Qt::MSWindowsFixedSizeDialogHint);

	SetupFormActions();

	mUI->checkBox_Music->setChecked(true);
	bool muted = !mUI->checkBox_Music->isChecked();
	InitializeMediaPlayer(muted);
}

DflwHackWindow::~DflwHackWindow()
{
	TurnOffHack();
	delete mUI;
}

void DflwHackWindow::toggle_HackEnable()
{
	if (!mHackLoaded)
	{
		QString path;
		if (!FindDFLWDLL(path))
		{
			Gui_ShowErrorPrompt("Unable to find DFLW.dll");
			return;
		}

		HANDLE dfHandle = FindProcess(L"Dflw.exe");

		if (!dfHandle)
		{
			Gui_ShowErrorPrompt("Unable to find running `Dflw.exe` process");
			return;
		}

		auto dllLoadStatus = LoadRemoteDLL(dfHandle, path.toStdWString().c_str());

		if (dllLoadStatus != LoadDLLStatus::cSUCCESS)
		{	
			QString errorStr;
			
			switch (dllLoadStatus)
			{
			case LoadDLLStatus::cFAIL_VIRTUALALLOC:
				errorStr = "Failed Virtual Allocation";
				break;
			case LoadDLLStatus::cFAIL_WRITEMEMORY:
				errorStr = "Failed Process Write Memory";
				break;
			case LoadDLLStatus::cFAIL_GETPROCADDRESS:
				errorStr = "Failed Get Process Address for kernel32";
				break;
			case LoadDLLStatus::cFAIL_CREATEREMOTETHREAD:
				errorStr = "Failed to Create Remote Thread";
				break;
			}

			Gui_ShowErrorPrompt("Unable to load hack DLL - Error: " + errorStr);
			return;
		}

		mHackLoaded = true;

		Gui_SetHackEnabledPicture(mHackLoaded);


	}
	else
	{
		TurnOffHack();
	}
}

void DflwHackWindow::toggle_GodMode()
{
	if (mHackLoaded)
	{
		ConnectToHack();

		mGodMode = !mGodMode;

		// TODO: Eventually I would imagine we want some sort of syn syn/ack ack interaction
		SendMessageOverPipe(cGOD_MODE);

		// Set the appropriate text
		Gui_SetElementTextEnabled(mUI->lineEdit_GodModeStatus, mGodMode);
	}
}

void DflwHackWindow::toggle_UnlimitedAmmo()
{
	if (mHackLoaded)
	{
		ConnectToHack();

		mUnlimitedAmmo = !mUnlimitedAmmo;

		// TODO: Eventually I would imagine we want some sort of syn syn/ack ack interaction
		SendMessageOverPipe(cUNLIMITED_AMMO);

		// Set the appropriate text
		Gui_SetElementTextEnabled(mUI->lineEdit_UnlimitedAmmoStatus, mUnlimitedAmmo);
	}
}

void DflwHackWindow::toggle_ESP()
{
	if (mHackLoaded)
	{
		ConnectToHack();

		mESP = !mESP;

		// TODO: Eventually I would imagine we want some sort of syn syn/ack ack interaction
		SendMessageOverPipe(cESP);

		// Set the appropriate text
		Gui_SetElementTextEnabled(mUI->lineEdit_ESPStatus, mESP);
	}
}

void DflwHackWindow::on_button_click_About()
{
	QMessageBox messageBox(QMessageBox::Icon::Information, "About", "", QMessageBox::Close);
	messageBox.setWindowIcon(QIcon(":/resources/DeltaForceLandWarriorHack.ico"));
	messageBox.setTextFormat(Qt::TextFormat::RichText);

	messageBox.setText("<b>Delta Force Land Warrior Hack</b><br/>"
				   "<b>Author</b>: Lifty<br/>");
	
	messageBox.setInformativeText("<b>Shortcuts</b>:<br/>"
							      "Ctrl+Shift+F1 : Enable Hack<br/>"
							      "Ctrl+Shift+F2 : Enable God Mode<br/>"
							      "Ctrl+Shift+F3 : Enable Unlimited Ammo<br/>"
							      "Ctrl+Shift+F4 : Enable ESP<br/>"
							      "<br/>"
							      "<b>Credits</b>:<br/>"
							      "Track: Tobu - Infectious [NCS Release]<br/>"
							      "Music provided by <a href=\"http://spoti.fi/NCS\">NoCopyrightSounds</a><br/>");
	messageBox.exec();
}

void DflwHackWindow::on_check_changed_Music(bool aNewState)
{
	mMusicPlayer.setMuted(!aNewState);
}

bool DflwHackWindow::nativeEvent(const QByteArray& eventType, void* message, long* result)
{
	MSG* msg = static_cast<MSG*>(message);
	
	if (msg->message == WM_HOTKEY)
	{
		dispatchGlobalShortcut(msg->wParam);
		return true;
	}
	
	return false;
}

void DflwHackWindow::SetupFormActions()
{
	// Visual Widget Actions
	QObject::connect(mUI->button_LoadUnloadHack, SIGNAL(clicked()), this, SLOT(toggle_HackEnable()));
	QObject::connect(mUI->button_GodMode, SIGNAL(clicked()), this, SLOT(toggle_GodMode()));
	QObject::connect(mUI->button_UnlimitedAmmo, SIGNAL(clicked()), this, SLOT(toggle_UnlimitedAmmo()));
	QObject::connect(mUI->button_ESP, SIGNAL(clicked()), this, SLOT(toggle_ESP()));
	QObject::connect(mUI->button_About, SIGNAL(clicked()), this, SLOT(on_button_click_About()));
	QObject::connect(mUI->checkBox_Music, SIGNAL(clicked(bool)), this, SLOT(on_check_changed_Music(bool)));

	// Global Keyboard shortcuts

	int enableHackID = 0;
	if (RegisterHotKey(HWND(winId()), enableHackID, MOD_CONTROL | MOD_SHIFT, VK_F1))
	{
		mShortcutCallbackMap.emplace(enableHackID, &DflwHackWindow::toggle_HackEnable);
	}
	else
	{
		QMessageBox::warning(this, "Warning", "Can't register hotkey CTRL+SHIFT+F1");
	}

	int godModeInt = 1;
	if (RegisterHotKey(HWND(winId()), godModeInt, MOD_CONTROL | MOD_SHIFT, VK_F2))
	{
		mShortcutCallbackMap.emplace(godModeInt, &DflwHackWindow::toggle_GodMode);
	}
	else
	{
		QMessageBox::warning(this, "Warning", "Can't register hotkey CTRL+SHIFT+F2");
	}

	int ammoID = 2;
	if (RegisterHotKey(HWND(winId()), ammoID, MOD_CONTROL | MOD_SHIFT, VK_F3))
	{
		mShortcutCallbackMap.emplace(ammoID, &DflwHackWindow::toggle_UnlimitedAmmo);
	}
	else
	{
		QMessageBox::warning(this, "Warning", "Can't register hotkey CTRL+SHIFT+F3");
	}

	int espID = 3;
	if (RegisterHotKey(HWND(winId()), espID, MOD_CONTROL | MOD_SHIFT, VK_F4))
	{
		mShortcutCallbackMap.emplace(espID, &DflwHackWindow::toggle_ESP);
	}
	else
	{
		QMessageBox::warning(this, "Warning", "Can't register hotkey CTRL+SHIFT+F4");
	}
}

void DflwHackWindow::InitializeMediaPlayer(bool aMuteValue)
{
	mMusicPlaylist.addMedia(QUrl("qrc:/resources/Tobu_Infectious_NCS_Release.mp3"));
	mMusicPlaylist.setPlaybackMode(QMediaPlaylist::Loop);
	
	mMusicPlayer.setPlaylist(&mMusicPlaylist);
	mMusicPlayer.setVolume(1);
	mMusicPlayer.play();

	mMusicPlayer.setMuted(aMuteValue);
}

bool DflwHackWindow::FindDFLWDLL(QString& aPath)
{
	bool found = false;

	wchar_t* fileName = L"DFLW";
	wchar_t* fileExt = L".dll";
	LPWSTR lpFilePart;
	wchar_t filename[MAX_PATH];

	if (SearchPath(NULL, fileName, fileExt, MAX_PATH, filename, &lpFilePart))
	{
		aPath = QString::fromStdWString(filename);
		found = true;
	}

	return found;
}

void DflwHackWindow::SendMessageOverPipe(PipeMessageType aMessageHeader)
{
	if (mPipeHandle != INVALID_HANDLE_VALUE)
	{
		PipeMessageType message[gPipeMessageLength] = { 0 };

		message[0] = aMessageHeader;

		unsigned long ulBytesWritten = 0;
		bool bWriteOk = WriteFile(mPipeHandle, (LPCVOID)& message, sizeof(message), &ulBytesWritten, NULL);
	}
}

void DflwHackWindow::TurnOffHack()
{
	// It sounds odd but we actually want to call "ConnectToHack" here as well. There is a potential case where the hack was loaded but never turned on, so we need to turn it on by connecting the pipe before we can user the quit command.
	ConnectToHack();

	// Send the quit message and then close the pipe
	SendMessageOverPipe(cQUIT);
	CloseHandle(mPipeHandle);
	mPipeHandle = INVALID_HANDLE_VALUE;
	
	mHackLoaded = false;
	mGodMode = false;
	mUnlimitedAmmo = false;
	mESP = false;

	Gui_SetHackEnabledPicture(mHackLoaded);

	Gui_SetHackOptionsEnabled(mHackLoaded);
}

bool DflwHackWindow::ConnectToHack()
{
	if (mHackLoaded)
	{
	if (mPipeHandle == INVALID_HANDLE_VALUE)
	{
		// Does pipe creation need to be offput 
		// create the named pipe handle
		mPipeHandle = CreateFile(gPipeHandle, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);

		// if everything ok set mode to message mode
		if (INVALID_HANDLE_VALUE == mPipeHandle)
		{
			// Connection to the pipe could not be created
			Gui_ShowErrorPrompt("Unable to create named pipe file");
			return false;
		}

		DWORD dwMode = PIPE_READMODE_MESSAGE;
		if (!SetNamedPipeHandleState(mPipeHandle, &dwMode, NULL, NULL))
		{
			// We have failed and should "bail out"
			CloseHandle(mPipeHandle);
			mPipeHandle = INVALID_HANDLE_VALUE;
			Gui_ShowErrorPrompt("Unable to set pipe mode to `MESSAGE`");
			return false;
		}
	}
	}

	return true;
}

void DflwHackWindow::dispatchGlobalShortcut(int aGlobalKeyboardShortcutID)
{
	auto callback = mShortcutCallbackMap.find(aGlobalKeyboardShortcutID);

	if (callback != mShortcutCallbackMap.end())
	{
		(this->*(callback->second))();
	}
}

void DflwHackWindow::Gui_SetHackEnabledPicture(bool aEnabled)
{
	if (aEnabled)
	{
		// Switch the icon of the button back
		mUI->button_LoadUnloadHack->setIcon(QIcon(":/resources/Lifty2.png"));

		// Update the status bar text
		mUI->lineEdit_LoadHackStatus->setText("Loaded");
	}
	else
	{
		// Switch the icon of the button back
		mUI->button_LoadUnloadHack->setIcon(QIcon(":/resources/Lifty1.png"));

		// Update the status bar text
		mUI->lineEdit_LoadHackStatus->setText("Unloaded");
	}
}

void DflwHackWindow::Gui_SetHackOptionsEnabled(bool aEnabled)
{
	Gui_SetElementTextEnabled(mUI->lineEdit_GodModeStatus, aEnabled);
	Gui_SetElementTextEnabled(mUI->lineEdit_UnlimitedAmmoStatus, aEnabled);
	Gui_SetElementTextEnabled(mUI->lineEdit_ESPStatus, aEnabled);
}

void DflwHackWindow::Gui_ShowErrorPrompt(QString aErrorString)
{
	QMessageBox messageBox(QMessageBox::Icon::Critical, "Error", aErrorString, QMessageBox::Close);
	messageBox.setWindowIcon(QIcon(":/resources/DeltaForceLandWarriorHack.ico"));
	messageBox.exec();
}

void DflwHackWindow::Gui_SetElementTextEnabled(QLineEdit* aLineEdit, bool aEnabled)
{
	QString text = (aEnabled) ? "Enabled" : "Disabled";
	aLineEdit->setText(text);
}
