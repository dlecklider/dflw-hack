#include "GameDataMgr.hpp"

#include "Util.hpp"

std::unique_ptr<GameDataMgr> gGameDataMgr = nullptr;

GameDataMgr::GameDataMgr(uintptr_t aBasePlayerPtr):
	mPlayers(),
	mSettings(),
	mPlayer(),
	mPlayerBasePtr(aBasePlayerPtr)
{
}

bool GameDataMgr::GetInMatch()
{
	return true;
}

const std::vector<Player>& GameDataMgr::GetOpponentDataList()
{
	// TODO: insert return statement here
	auto playerCount = util::ReadAddressOfLength(0x01172E60, 4);

	mPlayers.resize(playerCount); // Only ever expand

	for (int i = 0; i < playerCount; ++i)
	{
		//Player player;

		//player

		//mPlayers[0] = player;
	}

	return mPlayers;
}

const Player& GameDataMgr::GetPlayerData()
{
	mPlayer.mLocXYZ[0] = util::ReadAddressOfLength(mPlayerBasePtr, 4, { 0x24 });
	mPlayer.mLocXYZ[1] = util::ReadAddressOfLength(mPlayerBasePtr, 4, { 0x28 });
	mPlayer.mLocXYZ[2] = util::ReadAddressOfLength(mPlayerBasePtr, 4, { 0x2C });

	return mPlayer;
}

const SysSettings& GameDataMgr::GetSystemSettings()
{
	return mSettings;
}
