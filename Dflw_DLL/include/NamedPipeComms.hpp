#ifndef NAMED_PIPE_COMMS_HPP
#define NAMED_PIPE_COMMS_HPP

constexpr wchar_t* gPipeHandle = L"\\\\.\\pipe\\dflwliftypipe";
constexpr unsigned int gPipeMessageLength = 1;
typedef char PipeMessageType;

enum MessageContents
{
	cGOD_MODE = 0x01,
	cUNLIMITED_AMMO = 0x02,
	cESP = 0x04,
	cQUIT = 0x08
};

#endif
