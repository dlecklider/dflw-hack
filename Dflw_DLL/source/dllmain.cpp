#include <vector>

#define WIN32_LEAN_AND_MEAN 
#include "windows.h"
#include "NamedPipeComms.hpp"
#include "DflwHack.hpp"

HANDLE	hDllThread	=	INVALID_HANDLE_VALUE;
HMODULE	hDllModule	=	NULL;

DWORD WINAPI MainThread(LPVOID handle)
{
	bool gPipeExists = false;

	PipeMessageType message[gPipeMessageLength] = { 0 };
	bool bConnected = false;

	HANDLE hPipe = CreateNamedPipe(gPipeHandle,
		PIPE_ACCESS_DUPLEX,
		PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT,
		PIPE_UNLIMITED_INSTANCES,
		sizeof(message),
		0,
		0,
		NULL);

	if (hPipe == INVALID_HANDLE_VALUE) {
		// TODO: Do we have some method of alerting the user??
		return -1;
	}

	double waitTimeMs = 10;
	bool loopContinue = true;

	bConnected = ConnectNamedPipe(hPipe, NULL) ? true : (GetLastError() == ERROR_PIPE_CONNECTED);

	if(bConnected)
	{
		DflwHack gameHack;
		gameHack.Init();

		// Main loop, as long as we dont recieve a quit message we continue
		while (loopContinue)
		{
			unsigned long total_bytes = 0;
			// Peek to see if we have any new commands to process from the input pipe. If we do then we process them here.

			if (FALSE != PeekNamedPipe(hPipe, 0, 0, 0, &total_bytes, 0))
			{
				if (total_bytes > 0)
				{
					// We have a message to process
					// Read the message
					bool readOk = ReadFile(hPipe, &message, sizeof(message), &total_bytes, NULL);

					if (readOk && total_bytes > 0)
					{
						if (message[0] & cGOD_MODE)
						{
							gameHack.toggleGodMode();
						}

						if (message[0] & cUNLIMITED_AMMO)
						{
							gameHack.toggleUnlimitedAmmo();
						}

						if (message[0] & cESP)
						{
							gameHack.toggleESP();
						}

						if (message[0] & cQUIT)
						{
							loopContinue = !loopContinue;
						}
					}
				}
			}

			// Apply our settings, updated every 10ms
			gameHack.Update();

			Sleep(waitTimeMs);
		}
	}
	
	// Exit routine : close our thread and eject library
	if(gPipeExists)
	{
		DisconnectNamedPipe(hPipe); // Clean up pipe here
		gPipeExists = false;
	}

	FreeLibraryAndExitThread(hDllModule, 0);
	
	return 0;
}
 
BOOL APIENTRY DllMain( HMODULE hModule, DWORD  fdwReason, LPVOID lpReserved)
{
	if (fdwReason == DLL_PROCESS_ATTACH)
	{
		hDllModule = hModule;
		hDllThread = CreateThread(0, 0, &MainThread, hDllModule, 0, 0);
		CloseHandle(hDllThread);
	}
	else if (fdwReason == DLL_PROCESS_DETACH)
	{
	}

    return TRUE;
}
