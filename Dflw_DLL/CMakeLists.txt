include_directories(
  include
  ${DX7_INCLUDE_DIR}
  ${DETOURS_INCLUDE_DIR}
)

set(HEADERS
  "include/NamedPipeComms.hpp" 
  "include/DflwHack.hpp" 
  "include/HookDx7.hpp" 
  "include/Util.hpp"
  "include/GameDataMgr.hpp"
)

set(SOURCES
  "source/Dllmain.cpp" 
  "source/DflwHack.cpp" 
  "source/HookDx7.cpp"
  "source/Util.cpp"
  "source/GameDataMgr.cpp"
)

add_compile_definitions(_UNICODE UNICODE)

add_library(DFLW SHARED ${HEADERS} ${SOURCES})

set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS}")

target_link_directories(DFLW PRIVATE ${DETOURS_LIB_DIR})
target_link_libraries(DFLW detours.lib)

if(${BUILD_DETOURS})
  # Build Detours library before we try to link against it
  add_custom_command(TARGET DFLW 
    PRE_BUILD
    COMMAND SET ARGS DETOURS_TARGET_PROCESSOR=X86
    COMMAND cd ARGS ${DETOURS_DIR}/src
    COMMAND nmake ARGS 
    COMMAND cd ARGS ${CMAKE_CURRENT_SOURCE_DIR}
  )
endif()

install (TARGETS DFLW RUNTIME DESTINATION bin)