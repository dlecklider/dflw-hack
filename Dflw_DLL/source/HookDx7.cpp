#include "HookDx7.hpp"

#include <memory>

#include "detours.h"
#include "Util.hpp"
#include "GameDataMgr.hpp"

D3DCOLOR fontRed = RGB_MAKE(255, 0, 0);

typedef HRESULT(__stdcall* EndScene_t)(LPDIRECT3DDEVICE7);
EndScene_t OrigEndScene;

void DrawPlayerIfVisible(const Player& aMyPlayer, const Player& aOpponent, const SysSettings& aSettings)
{
	
}

HRESULT __stdcall myEndScene(LPDIRECT3DDEVICE7 aDevice)
{
	if (gGameDataMgr)
	{
		//bool inMatch = gDataMgr->GetInMatch();

		//if (inMatch)
		//{
		//	// Iterate over every opponent and get their position
		//	auto opponents = gDataMgr->GetOpponentDataList();

		//	// Get the players position
		//	// Get the view angle of the player
		//	auto myPlayer = gDataMgr->GetPlayerData();
		//	auto systemSettings = gDataMgr->GetSystemSettings();

		//	// For each opponent check if they are within view
		//	for (const auto& opponent : opponents)
		//	{
		//		DrawPlayerIfVisible(myPlayer, opponent, systemSettings);
		//	}
		//}

		// As a test see if we can get our position
		dx7::DrawBorderBox(50, 50, 200, 200, 3, fontRed, aDevice);
	}

	return OrigEndScene(aDevice);
}

void dx7::DrawBorderBox(int x, int y, int w, int h, int thickness, D3DCOLOR Colour, LPDIRECT3DDEVICE7 pDevice)
{
	//Top horiz line
	DrawFilledRect(x, y, w, thickness, Colour, pDevice);
	//Left vertical line
	DrawFilledRect(x, y, thickness, h, Colour, pDevice);
	//right vertical line
	DrawFilledRect((x + w), y, thickness, h, Colour, pDevice);
	//bottom horiz line
	DrawFilledRect(x, y + h, w + thickness, thickness, Colour, pDevice);
}

//We receive the 2-D Coordinates the colour and the device we want to use to draw those colours with
void dx7::DrawFilledRect(int x, int y, int w, int h, D3DCOLOR color, LPDIRECT3DDEVICE7 dev)
{
	//We create our rectangle to draw on screen
	D3DRECT BarRect = { x, y, x + w, y + h };
	//We clear that portion of the screen and display our rectangle
	dev->Clear(1, &BarRect, D3DCLEAR_TARGET | D3DCLEAR_TARGET, color, 0, 0);
}

void dx7::HookDevice(LPDIRECT3DDEVICE7 aDevice)
{
	if (aDevice)
	{
		OrigEndScene = (EndScene_t)util::GetInterfaceMethod((PVOID*)aDevice, 6); // Do I want to keep this method?

		DetourTransactionBegin();
		DetourUpdateThread(GetCurrentThread());
		DetourAttach(&(PVOID&)OrigEndScene, myEndScene);
		DetourTransactionCommit();
	}
}

void dx7::UnhookDevice(LPDIRECT3DDEVICE7 aDevice)
{
	if (aDevice)
	{
		if (OrigEndScene)
		{
			DetourTransactionBegin();
			DetourUpdateThread(GetCurrentThread());
			DetourDetach(&(PVOID&)OrigEndScene, myEndScene);
			DetourTransactionCommit();
		}
	}
}

LPDIRECT3DDEVICE7 dx7::GetD3Dim700Device()
{
	LPDIRECT3DDEVICE7 lpd3dDevice = nullptr;
	auto basePtr = ((IDirect3DDevice7**)0x0061474C);

	// Check to see if the directx 7 device is created yet
	if (*basePtr)
	{
		lpd3dDevice = *basePtr;
	}

	return lpd3dDevice;
}
