#include "DflwHack.hpp"

#include <Windows.h>
#include <memory>

#include "HookDx7.hpp"
#include "detours.h"
#include "Util.hpp"
#include "GameDataMgr.hpp"

DflwHack::DflwHack() :
	mAmmoCountMap(),
	mPlayerBasePtr(0x006A3788),
	mInitialValuesSet(false),
	mGodModeInput(false),
	mGodModeEnabled(false),
	mUnlimitedAmmoInput(false),
	mUnlimitedAmmoEnabled(false),
	mESPInput(false),
	mESPEnabled(false)
{
	mAmmoCountMap = {
		// Colt 45
		{0x008DB220, -1},
		{0x008DB320, -1},

		// Colt 45 Silenced
		{0x008DB224, -1},
		{0x008DB324, -1},

		// Water Pistol
		{0x008DB260, -1},
		{0x008DB360, -1},

		// Glock
		{0x008DB228, -1},
		{0x008DB328, -1},

		// AT4
		{0x008DB170, -1},
		{0x008DB370, -1},

		// Claymore
		{0x008DB184, -1},
		{0x008DB384, -1},

		// Satchel Charge
		{0x008DB180, -1},
		{0x008DB380, -1},

		// Impact Grenade
		{0x008DB174, -1},
		{0x008DB374, -1},

		// Timed Grenade
		{0x008DB178, -1},
		{0x008DB378, -1},

		// UMP
		{0x008DB258, -1},
		{0x008DB358, -1},

		// MP5
		{0x008DB254, -1},
		{0x008DB354, -1},

		// Calico
		{0x008DB25C, -1},
		{0x008DB35C, -1},

		// OICW
		{0x008DB24C, -1},
		{0x008DB34C, -1},

		// OICW Grenades
		{0x008DB220, -1},
		{0x008DB350, -1},

		// Water Rifle
		{0x008DB264, -1},
		{0x008DB364, -1},

		// Pancor
		{0x008DB268, -1},
		{0x008DB368, -1},

		// Steyr
		{0x008DB240, -1},
		{0x008DB340, -1},

		// Steyr Grenade
		{0x008DB144, -1},
		{0x008DB344, -1},

		// AK47
		{0x008DB248, -1},
		{0x008DB348, -1},

		// Primary Grenade Launcher
		{0x008DB2A0, -1},
		{0x008DB3A0, -1},

		// PSG1
		{0x008DB210, -1},
		{0x008DB310, -1},

		// Barret
		{0x008DB214, -1},
		{0x008DB314, -1},

		// M40
		{0x008DB21C, -1},
		{0x008DB31C, -1},

		// G11
		{0x008DB23C, -1},
		{0x008DB33C, -1},

		// FN Mag
		{0x008DB230, -1},
		{0x008DB330, -1},

		// M4 Masterkey
		{0x008DB234, -1},
		{0x008DB334, -1},

		// M4 Masterkey Shotgun
		{0x008DB238, -1},
		{0x008DB338, -1},

		// M249
		{0x008DB22C, -1},
		{0x008DB32C, -1}
	};
}

DflwHack::~DflwHack()
{
	// Upon destruction we want to write all of the values back to what they were before
	if (mInitialValuesSet)
	{
		resetAmmoCounts();
	}
}

void DflwHack::Init()
{
	// TODO: Implement
	mInitialValuesSet = true;

	// Read in all of the ammo values
	// TODO: Handle reading in and setting only the ammo values of guns that are currently in use.
	for (auto& ammoEntry : mAmmoCountMap)
	{
		ammoEntry.second = util::ReadAddressOfLength(ammoEntry.first, 1);
	}

	if (!gGameDataMgr)
	{
		gGameDataMgr = std::make_unique<GameDataMgr>(mPlayerBasePtr);
	}
}

void DflwHack::Update()
{
	// The following truth table describes what we are doing for each "feature"
	// i = input
	// e = enabled
	// 0 0 | Do nothing
	// 0 1 | Teardown
	// 1 0 | Setup and update
	// 1 1 | Perform update

	if (mGodModeInput)
	{
		// The user has specified they want this feature enabled. 
		// Check if we have any initialization to do
		if (!mGodModeEnabled)
		{
			// The first time this is being run; initialize
			mPreGodModeHealth = getPlayerHealth();
			mGodModeEnabled = true;
		}

		// Perform regular update "actions" (if any)
		/*                | For teh lulz
		                  v
		*/
		setPlayerHealth(1337);
	}
	else
	{
		// Perform any neccesary cleanup if we were last enabled
		if (mGodModeEnabled)
		{
			setPlayerHealth(mPreGodModeHealth);
			mGodModeEnabled = false;
		}
	}

	if (mUnlimitedAmmoInput)
	{
		// The user has specified they want this feature enabled. 
		// Check if we have any initialization to do
		if (!mUnlimitedAmmoEnabled)
		{
			// The first time this is being run; initialize
			mUnlimitedAmmoEnabled = true;
		}

		// Perform regular update "actions" (if any)
		setUnlimitedAmmo();
	}
	else
	{
		// Perform any neccesary cleanup if we were last enabled
		if (mUnlimitedAmmoEnabled)
		{
			resetAmmoCounts();
			mUnlimitedAmmoEnabled = false;
		}
	}

	if (mESPInput)
	{
		// The user has specified they want this feature enabled. 
		// Check if we have any initialization to do
		if (!mESPEnabled)
		{
			// The first time this is being run; initialize
			setDrawESP(true);
			mESPEnabled = true;
		}

		// Perform regular update "actions" (if any)
	}
	else
	{
		// Perform any neccesary cleanup if we were last enabled
		if (mESPEnabled)
		{
			setDrawESP(false);
			mESPEnabled = false;
		}
	}
}

int DflwHack::getPlayerHealth()
{
	return util::ReadAddressOfLength(mPlayerBasePtr, 2, { 0x8C });
}

void DflwHack::setPlayerHealth(int aVal)
{
	/*                                         | For teh lulz
	                                           v  
	*/
	util::WriteAddressOfLength(mPlayerBasePtr, aVal, 2, { 0x8C });
}

void DflwHack::setUnlimitedAmmo()
{
	for (const auto& ammoEntry : mAmmoCountMap)
	{
		util::WriteAddressOfLength(ammoEntry.first, 254, 1);
	}
}

void DflwHack::setDrawESP(bool aEnabled)
{
	auto dx7Device = dx7::GetD3Dim700Device();

	// Check to see if we have hooked directx yet
	if (aEnabled)
	{
		dx7::HookDevice(dx7Device);
	}
	else
	{
		dx7::UnhookDevice(dx7Device);
	}
}

void DflwHack::resetAmmoCounts()
{
	for (const auto& ammoEntry : mAmmoCountMap)
	{
		util::WriteAddressOfLength(ammoEntry.first, ammoEntry.second, 1);
	}
}
