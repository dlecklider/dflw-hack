#ifndef UTIL_HPP
#define UTIL_HPP

#include "windows.h"
#include <vector>

namespace util
{
	PVOID GetInterfaceMethod(PVOID intf, DWORD methodIndex);
	
	uintptr_t FindDMAAddr(uintptr_t aAddress, std::vector<unsigned int> offsets);

	void WriteAddressOfLength(uintptr_t aAddress, uint64_t aVal, unsigned int aBytes, std::vector<unsigned int> aOffsets = {});
	uint64_t ReadAddressOfLength(uintptr_t aAddress, unsigned int aBytes, std::vector<unsigned int> aOffsets = {});
}

#endif
