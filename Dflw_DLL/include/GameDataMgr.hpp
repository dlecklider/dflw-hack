#ifndef GAME_DATA_MGR_HPP
#define GAME_DATA_MGR_HPP

#include <memory>
#include <vector>

class GameDataMgr;
extern std::unique_ptr<GameDataMgr> gGameDataMgr;

struct Player
{
	float mLocXYZ[3];
	float mAzLookRad;
	float mElLookRad;
	bool mAlive;
};

struct SysSettings
{
	int mScreenHeightPx;
	int mScreenWidthPx;
	float mVertFOVAngle;
	float mHorzFOVAngle;
};

class GameDataMgr
{
public:
	GameDataMgr(uintptr_t aBasePlayerPtr);

	bool GetInMatch();
	const std::vector<Player>& GetOpponentDataList();
	const Player& GetPlayerData();
	const SysSettings& GetSystemSettings();

private:
	std::vector<Player> mPlayers;
	SysSettings mSettings;
	Player mPlayer;
	uintptr_t mPlayerBasePtr;
};

#endif


